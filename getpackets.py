#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

from src.extractor import Extractor

def processClients():
    files = sorted(os.listdir("clients"))
    for file1 in files:
        if file1[0] == ".":
            continue
        # skip binaries with .adata section
        #if file1.find("RE") < 0 and file1 >= "2008-10-08aRagexe.exe" and file1 <= "2009-03-18bRagexe.exe":
        #    continue
        # skip binary with condition in fill packets. Current interpreter cant do this
        if file1.find("2008-05-27") >= 0 or file1.find("2008-05-28aSakexe") >= 0 or file1.find("2008-06-03") >= 0:
            continue
        # some strange packet ids in code
        if file1 == "2008-06-17bSakexe.exe" or file1 == "2008-06-24bSakexe.exe" or file1 == "2008-07-01aSakexe.exe" or file1 == "2008-07-02aSakexe.exe":
            continue
        if file1 < "2003-10-28" or file1 > "2012-07-02":
            continue
        extractor.init(file1, "bpe_packets_info.txt")
        extractor.getpackets()


extractor = Extractor()
if len(sys.argv) > 1:
    extractor.init(sys.argv[1], "bpe_packets_info.txt")
    extractor.getpackets()
else:
    processClients()
