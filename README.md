Batch packet and information extractor for kro clients.

Forum topic: [http://herc.ws/board/topic/15589-batch-packets-client-info-extractor](http://herc.ws/board/topic/15589-batch-packets-client-info-extractor/)

Bug tracker: [https://gitlab.com/4144/bpe/issues](https://gitlab.com/4144/bpe/issues)


# Extract packets:

Supported client versions from 2003-10-28 to 2012-07-02 with some exceptions.

This mode can extract all packets only from old clients.

## Batch extract packets:

1. put clients in clients directory.

2. run ./getpackets.py

3. you will get extracted packets in output directory.

## Extract packets from one client:

1. put client in clients directory.

2. run ./getpackets.py CLIENTNAME.exe

3. you will get extracted packets in output directory.

# Extract information:

This mode can extract information mostly from new clients.

## Batch extract information:

1. put clients in clients directory.

2. run ./getinfo.py

3. you will get extracted information in output directory.

## Extract information from one client:

1. put client in clients directory.

2. run ./getinfo.py CLIENTNAME.exe

3. you will get extracted information in output directory.

# Create packets table hook script:

This mode can extract information mostly from new clients.

## Batch create hook scripts:

1. put clients in clients directory.

2. run ./getolly.py

3. you will get generated ollydbg scripts in output directory.

## Create hook script from one client:

1. put client in clients directory.

2. run ./getolly.py CLIENTNAME.exe

3. you will get generated ollydbg script in output directory.

