#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Section:
    def printNum(self, text, num):
        print "{0}: {1}, {2}".format(text, num, hex(num))


    def rawToVa(self, offset):
        return offset - self.rOffset + self.image_base + self.vOffset


    def printRawAddr(self, text, offset):
        return "{0}: {1}".format(text, hex(self.rawToVa(offset)))


    def getAddrSec(self, offset):
        return "{0}:{1}".format(self.name, hex(self.rawToVa(offset)))


    def printAddrInfo(self, offset):
        self.printNum("exe offset", offset)
        self.printNum("section offset", offset - self.rOffset)
        self.printNum("debug offset", self.rawToVa(offset))
        #self.printAll()


    def printAll(self):
        print "Section offsets:"
        print hex(self.vSize)
        print hex(self.vOffset)
        print hex(self.vEnd)
        print hex(self.rSize)
        print hex(self.rOffset)
        print hex(self.rEnd)
        print hex(self.vrDiff)
        print hex(self.align)
