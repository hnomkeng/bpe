#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.idatemplate import IdaTemplate

import os


class Ida:
    def addLine(self, w, text):
        w.write(text + "\n")


    def getScript(self, extractors):
        fileName = "output/decompile_{0}.sh".format(os.path.basename(os.path.abspath(".")))
        cmd = "wine ~/.wine/drive_c/IDA68/idaq.exe"
        with open(fileName, "wt") as w:
            w.write("#!/bin/bash\n")
            for extractor in extractors:
                #w.write("{0} -c -A -S\"idc/batch.idc {1} {2}\" -L{3}/ida.log -P+ -a- -R {3}.exe\n".format(
                w.write("run \"{0}\" \"{1}\" \"{2}\"\n".format(
                    extractor.exe.fileName,
                    hex(extractor.loginPollAddrVa),
                    hex(extractor.gamePollAddrVa)))
