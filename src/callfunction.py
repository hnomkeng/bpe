#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

class CallFunction:
    def __init__(self, addr):
        self.callCounter = 0
        self.callsFrom = set()
        self.collectCallsFrom = set()
        self.addr = addr
        # push count = calls
        self.counts = dict()
        self.adjustSp = 0

    def addCall(self, asm):
        self.callCounter = self.callCounter + 1
        self.callsFrom.add(asm.pos)
        if asm.pushCounter not in self.counts:
            self.counts[asm.pushCounter] = 0
        self.counts[asm.pushCounter] = self.counts[asm.pushCounter] + 1
        #print "push count before: {0} - {1}".format(hex(self.addr), asm.pushCounter)
        #print self.counts
        esp2 = asm.registers["esp"]
        esp1 = asm.startEsp - asm.stackAlign
        #print "stack: {0}-{1}={2}".format(esp1, esp2, esp1 - esp2)


    def checkPacket(self, packetId, len1, len2, flag):
        if len1 == 255 and len1 != len2:
            len1 = -1
        elif len1 == 4294967295:
            len1 = -1
            if len2 == 4294967295:
                len2 = -1
        elif len1 == 117440492:
            len1 = -1
            if len2 == 117440492:
                len2 = -1
        return packetId < 0xb00 and packetId > 0 and \
        flag >= -1 and flag <= 1 and \
        len1 >= -1 and len2 >= -1 and \
        len1 != 0 and len2 != 0


    def error(self, asm, text):
        asm.exe.log("{0}: {1}".format(text, self.adjustSp))
        asm.printAll()


    def resetFlags(self, asm):
        if asm.exe.isClientSkipFlag() == True:
            asm.beforeSecondCall = None
            asm.prevPacketId = 0
            asm.cxSet = 0
            if asm.debug == True:
                print "asm.beforeSecondCall = None"
                print "asm.prevPacketId = 0"
                print "asm.cxSet = False"


    def collectArgs(self, asm):
        # for 2011-10-05aRagexeRE.exe
        offset = asm.registers["esp"]
        #asm.printAll()
        if asm.debug == True:
            print "AdjustSp {0}".format(self.adjustSp)
        prevLens = asm.prevLens
        asm.prevLens = (0, 0)
        if asm.exe.isClientTooOld() == True:
            if asm.beforeSecondCall == True and asm.cxSet == True:
                print "Set all flags to wrong values"
                exit(0)
        if self.adjustSp == 4:
            data1 = asm.getMemory32(offset)
            if data1 > 0x1000000:
                if asm.exe.isClientTooOld() == True:
                    val1 = asm.getMemory32(data1)
                    if asm.beforeSecondCall == False or asm.beforeSecondCall == None:
                        if asm.cxSet == False and asm.exe.isClientSkipFlag() == False and asm.prevPacketId != 0 and asm.beforeSecondCall != None:
                            print "Cx not set before call1"
                            exit(0)
                        if asm.prevPacketId != 0:
                            print "Double prevPacketId set"
                            exit(0)
                        asm.prevPacketId = val1
                        asm.beforeSecondCall = True
                        asm.cxSet = False
                        self.collectCallsFrom.add(asm.pos)
                        if asm.debug == True:
                            print "asm.prevPacketId = {0}".format(asm.prevPacketId)
                            print "asm.cxSet = False"
                            print "asm.beforeSecondCall = True"
                            print "Use function call with adjustSp={0} (pointer)".format(self.adjustSp)
                        return
                    else:
                        if asm.beforeSecondCall == True and asm.exe.isClientSkipFlag() == False:
                            print "wrong flag1: {0}".format(asm.beforeSecondCall)
                            exit(0)
                if asm.debug == True:
                    print "Skip function call with adjustSp={0} (pointer)".format(self.adjustSp)
                return
            else:
                self.error(asm, "Error: unsupported function parameters. AdjustSp")
                exit(1)
        elif self.adjustSp == 8:
            data1 = asm.getMemory32(offset)
            data2 = asm.getMemory32(offset + 4)
            # both pointers
            if data1 > 0x1000000 and data2 > 0x1000000:
                # data2 is pointer to packet struct
                packetId = asm.getMemory32(data2)
                len1 = asm.getMemory32Sign(data2 + 4)
                len2 = asm.getMemory32Sign(data2 + 8)
                flag = asm.getMemory32(data2 + 12)
                if self.checkPacket(packetId, len1, len2, flag):
                    asm.addPacket(packetId, len1, len2, flag)
                    self.collectCallsFrom.add(asm.pos)
                    self.resetFlags(asm)
                    return
                packetId = asm.getMemory32(data1)
                len1 = asm.getMemory32Sign(data2)
                len2 = asm.getMemory32Sign(data2 + 4)
                flag = -1
                if self.checkPacket(packetId, len1, len2, flag):
                    asm.addPacket(packetId, len1, len2, flag)
                    self.collectCallsFrom.add(asm.pos)
                    self.resetFlags(asm)
                    return
                if packetId > 0 and len1 == 0:
                    if asm.exe.isClientTooOld() == True:
                        if asm.beforeSecondCall is None:
                            #if asm.cxSet == False:
                            #    print "Cx not set before call2"
                            #    exit(0)
                            if asm.prevPacketId != 0:
                                print "Double prevPacketId set"
                                exit(0)
                            asm.prevPacketId = packetId
                            asm.cxSet = False
                            if asm.debug == True:
                                print "asm.prevPacketId = {0}".format(asm.prevPacketId)
                                print "asm.cxSet = False"
                                print "Use function call with adjustSp={0} (pointers (N, 0) first call)".format(self.adjustSp)
                            self.collectCallsFrom.add(asm.pos)
                            return
                        else:
                            print "wrong flag2: {0}".format(asm.beforeSecondCall)
                            exit(0)
                    if asm.debug == True:
                        print "Skip function call with adjustSp={0} (pointers (N, 0))".format(self.adjustSp)
                    return
                if asm.getMemory32(data1) == 0 and asm.getMemory32(data2) == 0:
                    if asm.exe.isClientTooOld() == True:
                        if asm.beforeSecondCall == False:
                            asm.beforeSecondCall = True
                            asm.cxSet = False
                            if asm.debug == True:
                                print "asm.beforeSecondCall = True"
                                print "asm.cxSet = False"
                                print "Use function call with adjustSp={0} (pointers (N, 0) second call)".format(self.adjustSp)
                            return
                            self.collectCallsFrom.add(asm.pos)
                        else:
                            if asm.beforeSecondCall == True and asm.exe.isClientSkipFlag() == False:
                                print "wrong flag3: {0}".format(asm.beforeSecondCall)
                                exit(0)
                    if asm.debug == True:
                        print "Skip function call with adjustSp={0} (pointers zero)".format(self.adjustSp)
                    return
                if asm.debug == True:
                    print "Skip function call with adjustSp={0} (pointers)".format(self.adjustSp)
            else:
                len1 = data1
                len2 = data2
                if len1 >= -1 and len2 >= -1 and \
                    len1 != 0 and len2 != 0:
                    asm.prevLens = (len1, len2)
                    self.collectCallsFrom.add(asm.pos)
                    if asm.debug == True:
                        print "Use lens only in function call with adjustSp={0} (lens)".format(self.adjustSp)
                    return
            self.error(asm, "Error: unsupported function parameters. AdjustSp")
            exit(1)
        elif self.adjustSp == 12:
            data1 = asm.getMemory32(offset)
            data2 = asm.getMemory32(offset + 4)
            data3 = asm.getMemory32(offset + 8)
            if data1 > 0x1000000 and data2 > 0x1000000 and data3 > 0x1000000:
                val1 = asm.getMemory32(data1)
                val2 = asm.getMemory32(data2)
                val3 = asm.getMemory32(data3)
                if val1 == 0 and val3 == 0:
                    packetId = val2
                    len1 = prevLens[0]
                    len2 = prevLens[0]
                    flag = -1
                    asm.addPacket(packetId, len1, len2, flag)
                    self.collectCallsFrom.add(asm.pos)
                    self.resetFlags(asm)
                    return
                elif val2 != 0 and val3 != 0:
                    val4 = asm.getMemory32(data3 + 4)
                    flag = -1
                    if self.checkPacket(val2, val3, val4, flag):
                        asm.addPacket(val2, val3, val4, flag)
                        self.collectCallsFrom.add(asm.pos)
                        self.resetFlags(asm)
                        return
                    if asm.debug == True:
                        print "Skip function call with adjustSp={0} (0, N, M)".format(self.adjustSp)
                    return
            self.error(asm, "Error: unsupported function parameters. AdjustSp")
            exit(1)
        elif self.adjustSp == 16:
            self.collectCallsFrom.add(asm.pos)
            self.resetFlags(asm)
            data1 = asm.getMemory32(offset)
            data2 = asm.getMemory32Sign(offset + 4)
            data3 = asm.getMemory32Sign(offset + 8)
            data4 = asm.getMemory32(offset + 12)
            if data1 > 0x1000000 and data2 == 0 and data3 > 0x1000000 and data4 > 0x1000000:
                if asm.debug == True:
                    print "Skip function call with adjustSp={0}".format(self.adjustSp)
                return
            asm.addPacket(
                data1,
                data2,
                data3,
                data4
            )
        elif self.adjustSp == 0:
            # skip if function ignored
            if asm.debug == True:
                print "Skip function call with adjustSp={0}".format(self.adjustSp)
            pass
        else:
            self.error(asm, "Error: Unsupported adjustSp")
            exit(1)
            for i in range(0, self.adjustSp / 4):
                offset = asm.registers["esp"] + i * 4
                data = asm.getMemory32(offset)
                if data > 0x1000000:
                    print "stack pointer data: {0}: {1}".format(hex(offset), hex(data))
                    offset = data
                    data = asm.getMemory32(offset)
                print "stack data: {0}: {1}".format(hex(offset), data)
