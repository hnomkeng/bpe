#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.idatemplate import IdaTemplate


class Olly:
    def addLine(self, w, text):
        w.write(text + "\n")


    def getScript(self, extractors):
        template = IdaTemplate("file.txt", "block.txt")
        for extractor in extractors:
            template.addLine(extractor.ollyScript, extractor.cleanFileName)
        template.save("output", "extract_packets_{0}_{1}.txt", "cobmined")
