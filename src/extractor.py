#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.asm import Asm
from src.exe import Exe
from src.math import Math
from src.idatemplate import IdaTemplate

import os


class Extractor:
    template = \
        "\xE8\xAB\xAB\xAB\xAB" \
      + "\x8B\xC8"          \
      + "\xE8\xAB\xAB\xAB\xAB"


    def __init__(self):
        self.offset1 = None
        self.asmEndPos = 0
        self.hookAddr = 0
        self.hookAddr2 = 0
        self.windowFunc = 0
        self.addrList = []
        self.ollyScript = ""
        self.loginPollAddrVa = 0
        self.gamePollAddrVa = 0


    def close(self):
        self.exe.close()


    def addRawAddr(self, name, addr, addFlag):
        self.exe.printRawAddr(name, addr)
        if addFlag == True:
            self.addrList.append((name, hex(self.exe.rawToVa(addr))))


    def addVaAddr(self, name, addr, addFlag):
        self.exe.log("{0}: {1}".format(name, hex(addr)))
        if addFlag == True:
            self.addrList.append((name, hex(addr)))


    def getAddr(self, offset, addrOffset, instructionOffset):
        ptr = self.exe.read(offset + addrOffset, 4, "V")
        return Math.sumOffset(offset + instructionOffset, ptr)


    def run4Step(self, code, num, addrOffset, instructionOffset):
        offset = self.exe.code1(code, "\xAB", self.initPacketMapFunction, self.initPacketMapFunction + 0x140)
        if offset != False:
            self.addRawAddr("peek4.{0}".format(num), offset, False)
            self.initPacketLenWithClientFunction = self.getAddr(offset, addrOffset, instructionOffset)
        return offset


    def run4StepAlone(self, code, num, addrOffset, instructionOffset):
        offset = self.exe.code1(code, "\xAB")
        if offset != False:
            self.addRawAddr("peek4.{0}".format(num), offset, False)
            self.initPacketLenWithClientFunction = self.getAddr(offset, addrOffset, instructionOffset)
        return offset


    def getBasicInfo(self):
        # Step 1 - Find the GetPacketSize function call
        code = \
            self.template \
          + "\x50"    \
          + self.template \
          + "\x6A\x01" \
          + self.template \
          + "\x6A\x06"
        offset = self.exe.code1(code, "\xAB")
        if offset == False:
            code = \
                self.template \
              + "\x50"    \
              + self.template \
              + "\x6A\x01" \
              + self.template \
              + "\xe9"
            offset = self.exe.code1(code, "\xAB")
        if offset != False:
            self.offset1 = offset
            self.addRawAddr("peek1", offset, False)
            self.instanceR = self.getAddr(offset, 1, 5)
            self.addRawAddr("CRagConnection::instanceR", self.instanceR, True)

            # Step 2a - Go Inside the GetPacketSize function
            offset = offset + len(self.template) - 4
            # addr + offset + 4 in 32 bit math
            offset = self.getAddr(offset, 0, 4)
            self.getPacketSizeFunction = offset
            self.addRawAddr("CRagConnection::GetPacketSize", self.getPacketSizeFunction, True)
            code = \
                "\xB9\xAB\xAB\xAB\x00" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\xAB\x04"
            offset0 = offset
            offset = self.exe.code1(code, "\xAB", offset, offset + 0x80)
            if offset == False:
                offset = offset0
                code = \
                    "\xB9\xAB\xAB\xAB\x01" \
                    "\xE8\xAB\xAB\xAB\xAB" \
                    "\x8B\xAB\x04"
                offset = self.exe.code1(code, "\xAB", offset, offset + 0x80)
        else:
            # 1.2
            # push 1
            # call CRagConnection::instanceR
            # mov ecx, eax
            # call CConnection::SetBlock (probably)
            # push 6
            code = \
                "\x6a\x01" \
                "\xe8\xAB\xAB\xAB\xAB" \
                "\x8B\xC8" \
                "\xe8\xAB\xAB\xAB\xAB" \
                "\x6A\x06"
            offset = self.exe.code1(code, "\xAB")
            if offset != False:
                self.offset1 = offset
                self.addRawAddr("peek1.2", offset, False)
                self.instanceR = self.getAddr(offset, 3, 7)
                self.addRawAddr("CRagConnection::instanceR", self.instanceR, True)
                # skip step 2 and 3 becase no signatures for it. Go to 4.
                offset = False
            else:
                # in very old client no way to find any functions except initPacketLenWithClient
                # but functions can be wrong :(
                if self.exe.client_date <= 20031124:
                    # mov [esi + N], ebx
                    # mov [esi + N], ebx
                    # mov dword ptr [esi], N
                    # call initPacketLenWithClient
                    # mov ecx, [ebp + N]
                    offset = self.run4StepAlone(
                        "\x89\x9E\xAB\xAB\xAB\xAB" \
                        "\x89\x9E\xAB\xAB\xAB\xAB" \
                        "\xC7\x06\xAB\xAB\xAB\xAB" \
                        "\xE8\xAB\xAB\xAB\xAB" \
                        "\x8B\x4D\xAB",
                        8,
                        19, 23)
                elif self.exe.client_date <= 20041207:
                    # mov [esi + N], M
                    # mov [esi + N], M
                    # mov dword ptr [esi], N
                    # call initPacketLenWithClient
                    # mov ecx, [ebp + N]
                    offset = self.run4StepAlone(
                        "\x89\x9e\xAB\xAB\xAB\xAB" \
                        "\x89\x9e\xAB\xAB\xAB\xAB" \
                        "\xC7\x06\xAB\xAB\xAB\xAB" \
                        "\xE8\xAB\xAB\xAB\xAB" \
                        "\x8B\x4D\xAB",
                        7,
                        19, 23)
                    # mov ecx, esi
                    # mov byte ptr [ebp + N], M
                    # mov dword ptr [esi], N
                    # call initPacketLenWithClient
                    # mov ecx, [ebp + N]
                    if offset == False:
                        offset = self.run4StepAlone(
                            "\x8B\xCE" \
                            "\xC6\x45\xAB\xAB" \
                            "\xC7\x06\xAB\xAB\xAB\xAB" \
                            "\xE8\xAB\xAB\xAB\xAB" \
                            "\x8B\x4D\xAB",
                            9,
                            13, 17)
                if offset == False:
                    self.exe.log("failed in 1")
                    return False
                self.addRawAddr("CRagConnection::InitPacketLenWithClient", self.initPacketLenWithClientFunction, True)
                return True
        if offset != False:
            self.addRawAddr("peek2 (mov ecx, offset g_PacketLenMap)", offset, False)
            #Step 2c - Extract the g_PacketLenMap assignment
            self.gPacketLenMap = self.exe.read(offset + 1, 4)
            self.pktLenFunction = self.getAddr(offset, 6, 10)
            self.addRawAddr("pktLenFunction", self.pktLenFunction, True)

            # 3
            code = \
                self.gPacketLenMap + \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x68\xAB\xAB\xAB\x00" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x59" \
                "\xC3"
            offset = self.exe.code1(code, "\xAB")
            if offset != False:
                self.addRawAddr("peek3 (mov ecx, offset g_PacketLenMap)", offset, False)
                self.gPacketLenMapAddr = self.exe.read(offset, 4, "V")
                self.addVaAddr("g_PacketLenMap", self.gPacketLenMapAddr, True)
                offset = offset - 1
                self.initPacketMapFunction = self.getAddr(offset, 6, 10)
            else:
                # 3.2
                # mov RR, g_packetLenMap
                # call CRagConnection::InitPacketMap
                # push addr1
                # call addr2
                # add esp, N
                # mov esp, ebp
                # pop ebp
                # retn
                code = \
                    self.gPacketLenMap + \
                    "\xE8\xAB\xAB\xAB\xAB" \
                    "\x68\xAB\xAB\xAB\x00" \
                    "\xE8\xAB\xAB\xAB\xAB" \
                    "\x83\xC4\xAB" \
                    "\x8B\xE5" \
                    "\x5d" \
                    "\xC3"
                offset = self.exe.code1(code, "\xAB")
                if offset == False:
                    self.exe.log("failed in 3")
                    return False
                self.addRawAddr("peek3.2", offset, False)
                offset = offset - 1
                self.initPacketMapFunction = self.getAddr(offset, 6, 10)

        else:
            self.gPacketLenMap = None
            # search in CRagConnection::instanceR for call to InitPacketMap
            # ...
            # 2a.2
            # call CRagConnection::InitPacketMap
            # push addr1
            # call addr2
            code = \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x68\xAB\xAB\xAB\x00" \
                "\xE8\xAB\xAB\xAB\xAB"
            offset = self.exe.code1(code, "\xAB", self.instanceR, self.instanceR + 0x50)
            if offset == False:
                self.exe.log("failed in 2a")
                return False
            self.addRawAddr("peek2a.2", offset, False)
            self.initPacketMapFunction = self.getAddr(offset, 1, 5)

        # 4
        self.addRawAddr("CRagConnection::InitPacketMap", self.initPacketMapFunction, True)
        # peek step 4
        res = self.run4Step(
            "\x8B\xCE" \
            "\xE8\xAB\xAB\xAB\xAB" \
            "\xC7",
            1,
            3, 7)
        if res == False:
            # 4c.2
            # mov ecx, esi
            # mov [ebp + N], ebx
            # call initPacketLenWithClient
            # mov ecx, [ebp + N]
            res = self.run4Step(
                "\x8B\xCE" \
                "\x89\x5D\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\x4D\xAB",
                2,
                6, 10)
        if res == False:
            # 4c.3
            # mov ecx, esi
            # mov [ebp + N], M
            # call initPacketLenWithClient
            # mov ecx, [ebp + N]
            res = self.run4Step(
                "\x8B\xCE" \
                "\xC7\x45\xAB\xAB\xAB\xAB\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\x4D\xAB",
                3,
                10, 14)
        if res == False:
            # 4c.4
            # mov [ebp + N], M
            # mov dword ptr [esi], N
            # call initPacketLenWithClient
            # mov ecx, [ebp + N]
            res = self.run4Step(
                "\xC7\x45\xAB\xAB\xAB\xAB\xAB" \
                "\xC7\x06\xAB\xAB\xAB\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\x4D\xAB",
                4,
                14, 18)
        if res == False:
            # 4c.5
            # mov [esp + N], M
            # mov dword ptr [esi], N
            # call initPacketLenWithClient
            # mov [esp + N], M
            res = self.run4Step(
                "\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB" \
                "\xC7\x06\xAB\xAB\xAB\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB",
                5,
                15, 19)
        if res == False:
            # 4c.6
            # mov [esp + N], M
            # mov dword ptr [esi], N
            # call initPacketLenWithClient
            # mov eax, esi
            # mov ecx, [esp + N]
            res = self.run4Step(
                "\xC7\x44\x24\xAB\xAB\xAB\xAB\xAB" \
                "\xC7\x06\xAB\xAB\xAB\xAB" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\x8B\xC6" \
                "\x8B\x4C\x24\xAB",
                6,
                15, 19)
        if res == False:
            self.exe.log("failed in 4")
            return False
        self.addRawAddr("CRagConnection::InitPacketLenWithClient", self.initPacketLenWithClientFunction, True)
        return True


    def walkUntilRet(self):
        asm = Asm()
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        #asm.printLine()
        while asm.move() == True:
            #asm.printLine()
            pass
        self.asmEndPos = asm.pos
        self.addRawAddr("Last asm walk addr", self.asmEndPos, False)
        print "Asm code walked"


    def runAsmSimple(self):
        asm = Asm()
        asm.debug = False
        asm.debugMemory = False
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        asm.initState(0)
        #asm.printRegisters()
        #asm.printLine()
        asm.run()
        #asm.printRegisters()
        while asm.move() == True:
            #asm.printLine()
            asm.run()
            #asm.printRegisters()
            pass
        self.stackAlign = asm.popCounter
        self.callFunctions = asm.callFunctions
        self.allCallFunctions = asm.callFunctions
        self.allCallAddrs = asm.allCallAddrs
        print "Asm simple code complete"
        #print asm.memory


    def runAsmCollect(self):
        asm = Asm()
        asm.debug = False
        asm.debugMemory = False
        asm.stackAlign = self.stackAlign
        asm.exe = self.exe
        asm.init(self.initPacketLenWithClientFunction)
        asm.initState(1)
        asm.callFunctions = self.callFunctions
        asm.allCallFunctions = self.allCallFunctions
        #asm.printRegisters()
        #asm.printMemory()
        #asm.printLine()
        asm.run()
        while asm.move() == True:
            asm.run()
            pass
        self.packets = asm.packets
        self.callFunctions = asm.callFunctions
        print "Asm collector code complete"


    def parseCallStack(self):
        correctFunctions = dict()
        for addr in self.callFunctions:
            callFunction = self.callFunctions[addr]
            if callFunction.callCounter < 10:
                continue
            correctCounts = dict()
            tmpPushes = 0
            # walk all push counts and remove used very rare
            for pushCount in callFunction.counts:
                num = callFunction.counts[pushCount]
                if num > 10:
                    correctCounts[pushCount] = num
                    tmpPushes = pushCount
            callFunction.counts = correctCounts
            # need at least two different function calls in initPacketMapWithClient
            if len(callFunction.counts) > 1:
                self.exe.log("Error. Different number of pushes before function call: {0}\n{1}".format(self.exe.getAddrSec(addr), callFunction.counts))
                exit(1)
            callFunction.adjustSp = tmpPushes
            #print "addr {0}, pushes {1}".format(addr, callFunction.adjustSp)
            correctFunctions[addr] = callFunction
        self.callFunctions = correctFunctions


    def checkPackets(self):
        error = False
        for key in self.callFunctions:
            func = self.callFunctions[key]
            if len(func.collectCallsFrom) < 1:
                continue
            for calladdr in func.callsFrom:
                if calladdr not in func.collectCallsFrom:
                    print "Error function was not called from addr: {0}".format(self.exe.getAddrSec(calladdr))
                    error = True
        if error == True:
            exit(1)


        if len(self.packets) < 380:
            self.exe.log("Error. too small packets amount: {0}".format(len(self.packets)))
            exit(1)


    def savePackets(self):
        self.exe.log("Packets number: {0}".format(len(self.packets)))
        processedKeys = dict()
        with open("{0}/bpe_data_{1}.ini".format(self.exe.outDir, self.exe.client_date), "wt") as w:
            for packet in self.packets:
                packetId = hex(packet[0]).upper()
                if packet[0] in processedKeys:
                    newLen = processedKeys[packet[0]][1]
                    if newLen == packet[1]:
                        continue
                    self.exe.log("Warning. Duplicate packet with different size: {0}: {1} vs {2}".format(hex(packet[0]), packet[1], newLen))
                while len(packetId) < 6:
                    packetId = packetId[:2] + "0" + packetId[2:]
                packetId = packetId[0] + "x" + packetId[2:]
                w.write("{0},{1},{2},{3}\n".format(packetId, packet[1], packet[2], packet[3]))
                processedKeys[packet[0]] = packet
        processedKeys = dict()
        cleanPackets = dict()
        for packet in self.packets:
            cleanPackets[packet[0]] = packet
        with open("{0}/bpe_PacketLengths_{1}.ini".format(self.exe.outDir, self.exe.client_date), "wt") as w:
            w.write("[Packet_Lengths]\r\n")
            for packet in self.packets:
                packet = cleanPackets[packet[0]]
                packetId = hex(packet[0]).upper()
                if packet[0] in processedKeys:
                    newLen = processedKeys[packet[0]][1]
                    if newLen == packet[1]:
                        continue
                    self.exe.log("Warning. Duplicate packet with different size: {0}: {1} vs {2}".format(hex(packet[0]), packet[1], newLen))
                while len(packetId) < 6:
                    packetId = packetId[:2] + "0" + packetId[2:]
                packetId = packetId[0] + "x" + packetId[2:]
                w.write("{0} = {1}\r\n".format(packetId, packet[1]))
                processedKeys[packet[0]] = packet
            w.write("[Shuffle_Packets]\r\n")


    def searchCombo(self):
        if self.offset1 == None:
            self.comboAddr = 0
            self.exe.log("Packet keys look like not supported. Ignoring combofunction.")
            return
        # MOV ecx, DS:[ADDR1]
        # PUSH 1
        # CALL combofunction
        code = \
            "\x8B\x0D\xAB\xAB\xAB\x00" \
            "\x6A\x01" \
            "\xE8"
        offset = self.exe.code1(code, "\xAB", self.offset1 - 0x100, self.offset1)
        if offset == False:
            # b
            code = \
                "\x8B\x0D\xAB\xAB\xAB\x01" \
                "\x6A\x01" \
                "\xE8"
            offset = self.exe.code1(code, "\xAB", self.offset1 - 0x100, self.offset1)
        if offset == False:
            self.exe.log("Error: failed in seach combofunction")
            self.comboAddr = 0
            return
        self.comboAddr = self.getAddr(offset, 9, 13)
        self.comboFormat = 1
        self.addRawAddr("comboFunction", self.comboAddr, True)


    def searchLoginPacketHandler(self):
        #< 2017-11-XX
        # call WindowFunc
        # mov ecx, esi
        # call CLoginMode::PollNetworkStatus
        # mov ecx, esi
        # call CLoginMode::func
        # mov ecx, offset addr1
        # call func1
        # mov ecx, g_windowMgr
        # call UIWindowMgr::func2
        # mov ecx, esi
        # call addr3
        # cmp [esi + N], 0
        # jz addr4
        code = \
            "\xE8\xAB\xAB\xAB\x00" \
            "\x8B\xAB" \
            "\xE8\xAB\xAB\x00\x00" \
            "\x8B\xAB" \
            "\xE8\xAB\xAB\xAB\xFF" \
            "\xB9\xAB\xAB\xAB\x00" \
            "\xE8\xAB\xAB\xAB\xAB" \
            "\xB9\xAB\xAB\xAB\x00" \
            "\xE8\xAB\xAB\xAB\xFF" \
            "\x8B\xAB" \
            "\xE8\xAB\xAB\xAB\xFF" \
            "\x83\xAB\xAB\x00" \
            "\x74\xAB"
        offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2003-10-28
            # call WindowFunc
            # mov ecx, esi
            # call CLoginMode::PollNetworkStatus
            # mov eax, A
            # test eax, eax
            # jz addr
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\x00\x00" \
                "\xA1\xAB\xAB\xAB\x00" \
                "\x85\xAB" \
                "\x74\xAB"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2004-06-21
            # call WindowFunc
            # mov ecx, ebx
            # call CLoginMode::PollNetworkStatus
            # mov eax, [ebx + A]
            # cmp eax, B
            # jz addr
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\x00\x00" \
                "\x8B\xAB\xAB" \
                "\x83\xAB\xAB" \
                "\x74\xAB"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2011-11-04
            # call WindowFunc
            # mov ecx, esi
            # call CLoginMode::PollNetworkStatus
            # mov ecx, esi
            # call CLoginMode::func
            # mov ecx, offset addr1
            # call func1
            # mov ecx, g_windowMgr
            # call UIWindowMgr::func2
            # mov ecx, esi
            # call addr3
            # cmp [esi + N], 0
            # jz addr4
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\xFF\xFF" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\xAB\xFF" \
                "\xB9\xAB\xAB\xAB\x00" \
                "\xE8\xAB\xAB\xAB\xAB" \
                "\xB9\xAB\xAB\xAB\x00" \
                "\xE8\xAB\xAB\xAB\xFF" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\xAB\xFF" \
                "\x83\xAB\xAB\x00" \
                "\x74\xAB"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            self.exe.log("failed in search CLoginMode::PollNetworkStatus")
            exit(1)
            return
        self.windowFunc = self.getAddr(offset, 1, 5)
        self.loginPollAddr = self.getAddr(offset, 8, 12)
        self.loginPollAddrVa = self.exe.rawToVa(self.loginPollAddr)
        self.addRawAddr("WindowFunc", self.windowFunc, True)
        self.addRawAddr("CLoginMode::PollNetworkStatus", self.loginPollAddr, True)


    def searchMapPacketHandler(self):
        #< 2017-11-XX
        # call WindowFunc
        # mov ecx, esi
        # call CGameMode::PollNetworkStatus
        # call addr1
        # mov ecx, eax
        # call addr2
        code = \
            "\xE8\xAB\xAB\xAB\x00" \
            "\x8B\xAB" \
            "\xE8\xAB\xAB\xAB\x00" \
            "\xE8\xAB\xAB\xAB\xFF" \
            "\x8B\xAB" \
            "\xE8\xAB\xAB\xAB\xFF" \
            "\x85\xAB"
        offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2003-10-28
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # mov eax, [esi + A]
            # mov ebx, ds:timeGetTime
            # test eax, eax
            # jz addr
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\x01\x00" \
                "\x8B\xAB\xAB" \
                "\x8B\xAB\xAB\xAB\xAB\x00" \
                "\x85\xAB" \
                "\x0F\x84\xAB\xAB\x00\x00"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2004-02-23
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # mov eax, [esi + A]
            # test eax, eax
            # jz addr
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\x01\x00" \
                "\x8B\xAB\xAB" \
                "\x85\xAB" \
                "\x0F\x84\xAB\xAB\x00\x00"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2007-03-12
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # mov eax, [esi + A]
            # test eax, eax
            # jz addr
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\x02\x00" \
                "\x8B\xAB\xAB" \
                "\x85\xAB" \
                "\x0F\x84\xAB\xAB\x00\x00"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2007-10-23
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # lea ecx, [esi + A]
            # call addr1
            # mov eax, [esi + B]
            # test eax, eax
            # jz addr
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8D\xAB\xAB\xAB\x00\x00" \
                "\xE8\xAB\xAB\xAB\xFF" \
                "\x8B\xAB\xAB" \
                "\x85\xAB" \
                "\x0F\x84\xAB\xAB\x00\x00"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2008-05-20
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # lea ecx, [esi + A]
            # call addr1
            # cmp dword ptr [esi + A], 0
            # jz addr
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\x03\x00" \
                "\x8D\xAB\xAB\xAB\x00\x00" \
                "\xE8\xAB\xAB\xAB\xFF" \
                "\x83\xAB\xAB\x00" \
                "\x0F\xAB\xAB\xAB\x00\x00"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2008-09-03
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # mov eax, [esi + A]
            # test eax, eax
            # jz addr
            # call addr1
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB\xAB" \
                "\x85\xC0" \
                "\x0F\xAB\xAB\xAB\x00\x00" \
                "\xE8\xAB\xAB\x00\x00"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2011-01-04
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # cmp dword ptr [esi + A], 0
            # jz addr
            # call addr2
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x83\xAB\xAB\x00" \
                "\x0F\xAB\xAB\xAB\x00\x00" \
                "\xE8\xAB\xAB\xAB\xFF"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2011-01-04
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # call addr1
            # cmp eax, 2
            # jnz addr2
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\x03\x00" \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x83\xAB\xAB" \
                "\x75\xAB"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            #2011-03-29
            # call WindowFunc
            # mov ecx, esi
            # call CGameMode::PollNetworkStatus
            # call addr1
            # mov ecx, eax
            # call addr2
            # test eax, eax
            # jnz addr3
            code = \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\x03\x00" \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x8B\xAB" \
                "\xE8\xAB\xAB\xAB\x00" \
                "\x85\xAB" \
                "\x75\xAB"
            offset = self.exe.code1(code, "\xAB")
        if offset == False:
            self.exe.log("failed in search CGameMode::PollNetworkStatus")
            exit(1)
            return
        if self.windowFunc != self.getAddr(offset, 1, 5):
            self.exe.log("failed in validation CLoginMode::PollNetworkStatus, CGameMode::PollNetworkStatus")
            exit(1)
            return
        self.gamePollAddr = self.getAddr(offset, 8, 12)
        self.gamePollAddrVa = self.exe.rawToVa(self.gamePollAddr)
        self.addRawAddr("CGameMode::PollNetworkStatus", self.gamePollAddr, True)


    def sarchOEP(self):
        # call main
        # jmp $+5
        # push 14h
        # push offset addr1
        code = \
            "\xE8\xAB\xAB\x00\x00" \
            "\xE9\x00\x00\x00\x00" \
            "\x6A\x14" \
            "\x68"
        offset = self.exe.code1(code, "\xAB")
        if offset == False:
            self.exe.log("failed in search OEP")
            return
#        self.OEPAddr = self.getAddr(offset, 1, 5)
        self.OEPAddr = offset
        self.addRawAddr("OEP", self.OEPAddr, True)


    def searchHookAddr(self):
#        if self.comboAddr == 0:
#            self.exe.log("Skip searchHookAddr, because no combofunction")
#            self.hookAddr = 0
#            self.hookAddr2 = 0
#            return
        addrSet = set()
        addrList = dict()
        for addr in self.allCallAddrs:
            if addr not in addrSet:
                addrSet.add(addr)
                addrList[addr] = 1
            else:
                addrList[addr] = addrList[addr] + 1
        addr = self.allCallAddrs[2]
        if addrList[addr] < 3:
            self.exe.log("Cant find hook2 function")
            exit(1)
        self.hookAddr2 = addr
        # push eax
        # lea ecx, [ebp + addr1]
        # call hookaddr
        # mov [ebp + addr2], 0FFFFFFFFh
        code = \
            "\x50" \
            "\x8D\x4D\xAB" \
            "\xE8\xAB\xAB\x00\x00" \
            "\xC7\x45\xAB\xFF\xFF\xFF\xFF"
        addrOffset = 5
        self.hookAddrOffset = 8
        offset = self.exe.code1(code, "\xAB", self.hookAddr2, self.hookAddr2 + 100)
        if offset == False:
            #2014-05-08
            # push eax
            # lea ecx, [esi + A]
            # push ecx
            # add edi, B
            # push edi
            # call hookaddr
            # add esp, C
            # mov [ebp + addr2], 0FFFFFFFFh
            code = \
                "\x50" \
                "\x8D\x4E\xAB" \
                "\x51" \
                "\x83\xAB\xAB" \
                "\xAB" \
                "\xE8\xAB\xAB\xFF\xFF" \
                "\x83\xAB\xAB" \
                "\xC7\x45\xAB\xFF\xFF\xFF\xFF"
            addrOffset = 10
            self.hookAddrOffset = 12
            offset = self.exe.code1(code, "\xAB", self.hookAddr2, self.hookAddr2 + 100)
        if offset == False:
            self.exe.log("failed in seach hook addr")
            self.addRawAddr("hookAddr2", self.hookAddr2, False)
            exit(1)
            return False
        self.hookAddr = self.getAddr(offset, addrOffset, addrOffset + 4)


    def searchSendPacket(self, errorExit):
        # 1. search for call pattern
        #2017-11-XX
        # push 2
        # call CRagConnection::instanceR
        # mov ecx, eax
        # call CRagConnection::SendPacket
        instanceOffset = 3
        sendOffset = 10
        code = \
            "\x6A\x02" + \
            "\xE8\xAB\xAB\xAB\xAB" + \
            "\x8B\xAB" + \
            "\xE8"
        offset = self.exe.code1(code, "\xAB", self.gamePollAddr, self.gamePollAddr + 0x10000)
        if offset == False:
            #2011-01-04
            # push 2
            # mov word ptr [esp + N + M], cx
            # call CRagConnection::instanceR
            # mov ecx, eax
            # call CRagConnection::SendPacket
            code = \
                "\x6A\x02" + \
                "\x66\xAB\xAB\xAB\xAB" + \
                "\xE8\xAB\xAB\xAB\xAB" + \
                "\x8B\xAB" + \
                "\xE8"
            instanceOffset = 8
            sendOffset = 15
            offset = self.exe.code1(code, "\xAB", self.gamePollAddr, self.gamePollAddr + 0x10000)
        if offset == False:
            #2013-01-15
            # push 2
            # mov word ptr [ebp + N], dx
            # call CRagConnection::instanceR
            # mov ecx, eax
            # call CRagConnection::SendPacket
            code = \
                "\x6A\x02" + \
                "\x66\xAB\xAB\xAB" + \
                "\xE8\xAB\xAB\xAB\xFF" + \
                "\x8B\xAB" + \
                "\xE8"
            instanceOffset = 7
            sendOffset = 14
            offset = self.exe.code1(code, "\xAB", self.gamePollAddr, self.gamePollAddr + 0x10000)
        if offset == False:
            self.exe.log("failed searchSendPacket in step 1.")
            if errorExit == True:
                exit(1)
            return False
        # 2. compare is first call is really CRagConnection::instanceR
        if self.getAddr(offset, instanceOffset, instanceOffset + 4) != self.instanceR:
            self.exe.log("failed searchSendPacket in step 2.")
            if errorExit == True:
                exit(1)
            return False
        self.sendPacket = self.getAddr(offset, sendOffset, sendOffset + 4)
        self.addRawAddr("CRagConnection::SendPacket", self.sendPacket, True)
        return True


    def searchShuffle(self):
        # seach first shuffle packet
        # jmp addr1
        # mov eax, packetId
        # mov [ebp + addr2], ax
        # lea eax, [ebp + addr3]
        # push eax
        # mov [ebp + addr4], bl
        # mov [ebp + addr5], ecx
        # push packetId
        # jmp addr6
        code = \
            "\xE9\xAB\xAB\xFF\xFF" + \
            "\xB8\xAB\xAB\x00\x00" + \
            "\x66\xAB\xAB\xAB\xAB\xFF\xFF" + \
            "\x8D\xAB\xAB\xAB\xFF\xFF" + \
            "\x50" + \
            "\x88\xAB\xAB\xAB\xFF\xFF" + \
            "\x89\xAB\xAB\xAB\xFF\xFF" + \
            "\x68\xAB\xAB\x00\x00" + \
            "\xE9\xAB\xAB\x00\x00"
        offset = self.exe.code1(code, "\xAB")
        if offset == False:
            self.exe.log("failed in search first shuffle packet")
            return
        self.shuffle0 = self.exe.read(offset + 6, 4, "V")
        self.exe.log("First shuffle packetId: {0}".format(hex(self.shuffle0)))


    def searchWindowMgr(self, errorExit):
        # 1. search NUMACCOUNT
        offset, section = self.exe.string("NUMACCOUNT")
        if offset == False:
            self.exe.log("failed in search NUMACCOUNT")
            exit(1)
            return
        self.numAccount = section.rawToVa(offset)
        # 2. search UIWindowMgr::MakeWindow
        # mov ecx, g_windowMgr
        # call UIWindowMgr::MakeWindow
        # push 0
        # push 0
        # push "NUMACCOUNT"
        code = \
            "\xB9\xAB\xAB\xAB\x00" + \
            "\xE8\xAB\xAB\xAB\xFF" + \
            "\x6A\x00" + \
            "\x6A\x00" + \
            "\x68" + self.exe.toHex(self.numAccount, 4)
        offset = self.exe.code1(code, "\xAB")
        if offset == False:
            self.exe.log("failed in search WindowMgr step 2.")
            if errorExit == True:
                exit(1)
            return
        self.gWindowMgr = self.exe.read(offset + 1, 4, "V")
        self.UIWindowMgrMakeWindow = self.getAddr(offset, 6, 10)
        self.addVaAddr("g_windowMgr", self.gWindowMgr, True)
        self.addRawAddr("UIWindowMgr::MakeWindow", self.UIWindowMgrMakeWindow, True)
        # 3. search MsgStr
        # push A
        # push B
        # mov ecx, edi
        # call UIWindow_Create
        # push C
        # call MsgStr
        code = \
            "\x68\xAB\x00\x00\x00" + \
            "\x68\xAB\x00\x00\x00" + \
            "\x8B\xAB" + \
            "\xE8\xAB\xAB\xAB\xFF" + \
            "\x68\xAB\xAB\x00\x00" + \
            "\xE8\xAB\xAB\xAB\x00"
        windowCreateOffset = 13
        msgStrOffset = 23
        offset = self.exe.code1(code, "\xAB", self.UIWindowMgrMakeWindow, self.UIWindowMgrMakeWindow + 0x10000)
        if offset == False:
            #2011-01-04
            # push A
            # push B
            # mov ecx, edi
            # call UIWindow_Create
            # lea eax, [ebx + D]
            # push eax
            # call MsgStr
            code = \
                "\x68\xAB\x00\x00\x00" + \
                "\x68\xAB\x00\x00\x00" + \
                "\x8B\xAB" + \
                "\xE8\xAB\xAB\xAB\xFF" + \
                "\x8D\xAB\xAB\xAB\x00\x00" + \
                "\xAB" + \
                "\xE8\xAB\xAB\xAB\x00"
            offset = self.exe.code1(code, "\xAB", self.UIWindowMgrMakeWindow, self.UIWindowMgrMakeWindow + 0x10000)
            windowCreateOffset = 13
            msgStrOffset = 25
        if offset == False:
            #2011-12-07
            # push A
            # push B
            # mov ecx, edi
            # lea ebp, [ebx + D]
            # call UIWindow_Create
            # push ebp
            # call MsgStr
            code = \
                "\x68\xAB\x00\x00\x00" + \
                "\x68\xAB\x00\x00\x00" + \
                "\x8B\xAB" + \
                "\x8D\xAB\xAB\xAB\x00\x00" + \
                "\xE8\xAB\xAB\xAB\xFF" + \
                "\xAB" + \
                "\xE8\xAB\xAB\xAB\x00"
            offset = self.exe.code1(code, "\xAB", self.UIWindowMgrMakeWindow, self.UIWindowMgrMakeWindow + 0x10000)
            windowCreateOffset = 19
            msgStrOffset = 25
        if offset == False:
            #2013-01-15
            # push A
            # push B
            # mov ecx, edi
            # call UIWindow_Create
            # mov eax, [ebp + C]
            # add eax, D
            # push eax
            # call MsgStr
            code = \
                "\x68\xAB\x00\x00\x00" + \
                "\x68\xAB\x00\x00\x00" + \
                "\x8B\xAB" + \
                "\xE8\xAB\xAB\xAB\xFF" + \
                "\x8B\xAB\xAB" + \
                "\x05\xAB\xAB\x00\x00" + \
                "\xAB" + \
                "\xE8\xAB\xAB\xAB\x00"
            offset = self.exe.code1(code, "\xAB", self.UIWindowMgrMakeWindow, self.UIWindowMgrMakeWindow + 0x10000)
            windowCreateOffset = 13
            msgStrOffset = 27
        if offset == False:
            #2015-01-21
            # push A
            # push B
            # mov ecx, edi
            # add esi, C
            # call UIWindow_Create
            # push esi
            # call MsgStr
            code = \
                "\x68\xAB\x00\x00\x00" + \
                "\x68\xAB\x00\x00\x00" + \
                "\x8B\xAB" + \
                "\x81\xAB\xAB\xAB\x00\x00" + \
                "\xE8\xAB\xAB\xAB\xFF" + \
                "\xAB" + \
                "\xE8\xAB\xAB\xAB\x00"
            offset = self.exe.code1(code, "\xAB", self.UIWindowMgrMakeWindow, self.UIWindowMgrMakeWindow + 0x10000)
            windowCreateOffset = 19
            msgStrOffset = 25
        if offset == False:
            #2017-09-27
            # push A
            # push B
            # mov ecx, edi
            # call UIWindow_Create
            # push [ebp + C]
            # call MsgStr
            code = \
                "\x68\xAB\x00\x00\x00" + \
                "\x68\xAB\x00\x00\x00" + \
                "\x8B\xAB" + \
                "\xE8\xAB\xAB\xAB\x00" + \
                "\xFF\xAB\xAB" + \
                "\xE8\xAB\xAB\xAB\x00"
            offset = self.exe.code1(code, "\xAB", self.UIWindowMgrMakeWindow, self.UIWindowMgrMakeWindow + 0x10000)
            windowCreateOffset = 13
            msgStrOffset = 21
        if offset == False:
            self.exe.log("failed in search MsgStr.")
            if errorExit == True:
                exit(1)
            return
        self.UIWindowCreate = self.getAddr(offset, windowCreateOffset, windowCreateOffset + 4)
        self.MsgStr = self.getAddr(offset, msgStrOffset, msgStrOffset + 4)
        self.addRawAddr("UIWindow::Create", self.UIWindowCreate, True)
        self.addRawAddr("MsgStr", self.MsgStr, True)


    def getHexAddr(self, addr):
        addr = hex(addr)[2:]
        return "{0}{1}".format("0" * (8 - len(addr)), addr)


    def getSimpleAddr(self, addr):
        addr = hex(self.exe.rawToVa(addr))[2:]
        return "{0}{1}".format("0" * (8 - len(addr)), addr)


    def addScriptCommand(self, w, cmd):
        w.write(cmd + "\n")
        self.ollyScript = self.ollyScript + cmd + "\n"
        print cmd


    def addScriptCommand2(self, w, cmd):
        w.write(cmd + "\n")
        print cmd


    def cleanupName(self, fileName):
        if fileName[-2:] == "_D":
            fileName = fileName[:-2]
        if fileName[-5:] == "_dump":
            fileName = fileName[:-5]
        if fileName[-3:] == "_DP":
            fileName = fileName[:-3]
        self.cleanFileName = fileName


    def saveOllyScript(self):
        if self.comboAddr == 0 or self.hookAddr == 0 or self.hookAddr2 == 0:
            self.exe.log("Skip ollyscript because no hooks of combo functions found.")
            return
        subName = os.path.basename(os.path.abspath("."))
        outDir = "output/olly"
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        self.cleanupName(self.exe.fileName)
        with open(outDir + "/extract_packets_{0}_{1}.txt".format(subName, self.exe.fileName), "wt") as w:
            print "-------------------------------"
            self.addScriptCommand2(w, "// packets table extractor v2")
            self.addScriptCommand(w, "// {0}.exe".format(self.exe.fileName))
            self.addScriptCommand(w, "mov STARTADDR, {0}".format(self.getSimpleAddr(self.initPacketLenWithClientFunction)))
            self.addScriptCommand(w, "mov EXITADDR,  {0}".format(self.getSimpleAddr(self.asmEndPos)))
            self.addScriptCommand(w, "mov HOOKADDR1, {0}".format(self.getSimpleAddr(self.hookAddr)))
            self.addScriptCommand(w, "mov HOOKADDR2, {0}".format(self.getSimpleAddr(self.hookAddr2)))
            self.addScriptCommand(w, "mov HOOKOFFSET1, {0}".format(self.getHexAddr(self.hookAddrOffset)))
            self.addScriptCommand(w, "mov HOOKOFFSET2, {0}".format(self.getHexAddr(4)))
            self.addScriptCommand(w, "mov COMBOADDR, {0}".format(self.getSimpleAddr(self.comboAddr)))
            self.addScriptCommand(w, "mov EXENAME, \"{0}\"".format(self.cleanFileName))
            self.addScriptCommand2(w, "#inc \"packetstable2.txt\"")
            print "-------------------------------"


    def saveIdaScript(self):
        template = IdaTemplate("file.idc", "name.idc")
        for addr in self.addrList:
            template.addLine(addr[1], addr[0])
        template.save("output/ida", "ida_{0}_{1}.idc", self.exe.fileName)


    def init(self, fileName, outFileName):
        exe = Exe()
        if exe.load(fileName, outFileName) == False:
            print "Skipping file {0}".format(fileName)
            exit(1)
        self.exe = exe
        parsed = self.getBasicInfo()
        if parsed == False:
            self.exe.log("Error: basic parsing info failed")
            exit(1)


    def getpackets(self):
        self.walkUntilRet()
        self.runAsmSimple()
        self.parseCallStack()
        self.runAsmCollect()
        self.checkPackets()
        self.savePackets()


    def getInfo(self):
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()
        self.searchSendPacket(False)
        self.searchCombo()
        self.sarchOEP()
        self.searchShuffle()
        self.searchWindowMgr(False)
        self.saveIdaScript()


    def getOlly(self):
        self.walkUntilRet()
        self.runAsmSimple()
        self.parseCallStack()
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()
        self.searchSendPacket(False)
        self.searchCombo()
        self.searchHookAddr()
        self.saveOllyScript()


    def getIda(self):
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()


    def getDebug(self):
        self.searchLoginPacketHandler()
        self.searchMapPacketHandler()
