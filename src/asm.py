#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from src.callfunction import CallFunction
from src.math import Math

import struct


def unimplimentedOpcode(asm):
    asm.printLine()
    cmd = asm.getWord(asm.pos)
    asm.exe.log("Error: Unimplimented opcode detected: {0}".format(hex(cmd)))
    exit(1)
    pass


def op_unset_reg(asm):
    register = asm.codeParams[0]
    asm.registers[register] = 0


def op_push_reg(asm):
    register = asm.codeParams[0]
    asm.registers["esp"] = asm.registers["esp"] - 4
    asm.setMemory32(asm.registers["esp"], asm.registers[register])
    if asm.runFlag == 0:
        asm.pushCounter = asm.pushCounter + 4


def op_push_big(asm):
    asm.registers["esp"] = asm.registers["esp"] - 4
    val = asm.exe.read(asm.pos + 1, 4, "V")
    asm.setMemory32(asm.registers["esp"], val)
    if asm.runFlag == 0:
        asm.pushCounter = asm.pushCounter + 4


def op_push_small(asm):
    asm.registers["esp"] = asm.registers["esp"] - 4
    val = Math.extendByteToInt(asm.exe.read(asm.pos + 1, 1, "b"))
    asm.setMemory32(asm.registers["esp"], val)
    if asm.runFlag == 0:
        asm.pushCounter = asm.pushCounter + 4


def op_pop_reg(asm):
    register = asm.codeParams[0]
    asm.registers[register] = asm.getMemory32(asm.registers["esp"])
    asm.registers["esp"] = asm.registers["esp"] + 4
    if asm.runFlag == 0:
        asm.popCounter = asm.popCounter + 4


def op_add_reg_byte(asm):
    register = asm.codeParams[0]
    val = asm.exe.read(asm.pos + asm.codeParams[1], 1, "b")
    asm.addRegister(register, val)


def op_sub_byte(asm):
    register = asm.codeParams[0]
    val = asm.exe.read(asm.pos + asm.codeParams[1], 1, "b")
    asm.subRegister(register, val)


def op_sub_int(asm):
    register = asm.codeParams[0]
    val = asm.exe.read(asm.pos + asm.codeParams[1], 4, "i")
    asm.subRegister(register, val)


def op_and_reg_byte(asm):
    register = asm.codeParams[0]
    val = Math.extendByteToInt(asm.exe.read(asm.pos + asm.codeParams[1], 1, "b"))
    asm.andRegister(register, val)


def op_or_reg_byte(asm):
    register = asm.codeParams[0]
    val = Math.extendByteToInt(asm.exe.read(asm.pos + asm.codeParams[1], 1, "b"))
    asm.orRegister(register, val)


def op_mov_reg_reg(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    asm.registers[register1] = asm.registers[register2]


def op_mov_reg_val(asm):
    register = asm.codeParams[0]
    val = asm.exe.read(asm.pos + asm.codeParams[1], 4, "i")
    asm.registers[register] = val
    if asm.lastRegValue != True:
        if register == "eax":
            asm.lastRegValue = val
        else:
            asm.lastRegValue = None
    if asm.exe.isClientTooOld() == True:
        if register == "ecx":
            if asm.runFlag == 1:
                if asm.beforeSecondCall is not None:
                    if asm.beforeSecondCall != True and (asm.beforeSecondCall != False or asm.exe.isClientSkipFlag() == False):
                        print "Error: wrong flag op_mov_reg_val: {0}".format(asm.beforeSecondCall)
                        exit(0)
                if asm.exe.isClientSkipFlag() == False and asm.cxSet == True:
                    print "Double cx set"
                    exit(0)
            if asm.beforeSecondCall != True:
                asm.beforeSecondCall = False
                if asm.debug == True:
                    print "asm.beforeSecondCall = False"
            asm.cxSet = True
            if asm.debug == True:
                print "asm.cxSet = True"


def op_mov_reg_ptr_int(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    offset = asm.registers[register2] + asm.exe.read(asm.pos + asm.codeParams[2], 4, "i")
    asm.registers[register1] = asm.getMemory32(offset)


def op_mov_reg_ptr_byte(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    offset = asm.registers[register2] + asm.exe.read(asm.pos + asm.codeParams[2], 1, "b")
    asm.registers[register1] = asm.getMemory32(offset)


def op_mov_reg_ptr_zero(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    offset = asm.registers[register2]
    asm.registers[register1] = asm.getMemory32(offset)


def op_mov_ptr_reg_byte(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    offset = asm.registers[register1] + asm.exe.read(asm.pos + asm.codeParams[2], 1, "b")
    val = asm.registers[register2]
    asm.setMemory32(offset, val)
    if asm.exe.isClientTooOld() == True:
        if register1 == "eax":
            if asm.beforeSecondCall == True or (asm.beforeSecondCall == None and asm.prevPacketId != 0 and asm.cxSet == False):
                asm.addPacket(asm.prevPacketId, val, val, -1)
                asm.prevPacketId = 0
                if asm.debug == True:
                    print "asm.prevPacketId = 0"
            else:
                if asm.runFlag == 1:
                    print "Error: wrong flag op_mov_ptr_reg_byte: {0}".format(asm.beforeSecondCall)
                    exit(0)
            asm.beforeSecondCall = None
            if asm.debug == True:
                print "asm.beforeSecondCall = None3"


def op_mov_ptr_reg_int(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    offset = asm.registers[register1] + asm.exe.read(asm.pos + asm.codeParams[2], 4, "i")
    asm.setMemory32(offset, asm.registers[register2])


def op_lea_reg_byte(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    asm.setRegister(register1, asm.registers[register2] + asm.exe.read(asm.pos + asm.codeParams[2], 1, "b"))


def op_lea_reg_zero(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    asm.setRegister(register1, asm.registers[register2])


def op_lea_reg_int(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    asm.setRegister(register1, asm.registers[register2] + asm.exe.read(asm.pos + asm.codeParams[2], 4, "I"))


def op_mov_ptr_reg_byte_int(asm):
    register = asm.codeParams[0]
    offset = asm.registers[register] + asm.exe.read(asm.pos + asm.codeParams[1], 1, "b")
    val = asm.exe.read(asm.pos + asm.codeParams[2], 4, "i")
    asm.setMemory32(offset, val)
    if asm.exe.isClientTooOld() == True:
        if register == "eax":
            if asm.beforeSecondCall == True or (asm.beforeSecondCall == None and asm.prevPacketId != 0 and asm.cxSet == False):
                asm.addPacket(asm.prevPacketId, val, val, -1)
                asm.prevPacketId = 0
                if asm.debug == True:
                    print "asm.prevPacketId = 0"
            else:
                if asm.runFlag == 1:
                    print "Error: wrong flag op_mov_ptr_reg_byte_int: {0}".format(asm.beforeSecondCall)
                    exit(0)
            asm.beforeSecondCall = None
            if asm.debug == True:
                print "asm.beforeSecondCall = None1"


def op_mov_ptr_reg_zero_int(asm):
    register = asm.codeParams[0]
    offset = asm.registers[register]
    val = asm.exe.read(asm.pos + asm.codeParams[1], 4, "i")
    asm.setMemory32(offset, val)
    if asm.exe.isClientTooOld() == True:
        if register == "eax":
            if asm.beforeSecondCall == True:
                asm.addPacket(asm.prevPacketId, val, val, -1)
                asm.prevPacketId = 0
                if asm.debug == True:
                    print "asm.prevPacketId = 0"
            else:
                if asm.runFlag == 1:
                    print "Error: wrong flag op_mov_ptr_reg_zero_int: {0}".format(asm.beforeSecondCall)
                    exit(0)
            asm.beforeSecondCall = None
            if asm.debug == True:
                print "asm.beforeSecondCall = None2"


def op_mov_ptr_reg_zero_reg(asm):
    register1 = asm.codeParams[0]
    register2 = asm.codeParams[1]
    offset = asm.registers[register1]
    val = asm.registers[register2]
    asm.setMemory32(offset, val)
    if asm.exe.isClientTooOld() == True:
        if register1 == "eax":
            if asm.beforeSecondCall == True:
                asm.addPacket(asm.prevPacketId, val, val, -1)
                asm.prevPacketId = 0
                if asm.debug == True:
                    print "asm.prevPacketId = 0"
            else:
                if asm.runFlag == 1:
                    print "Error: wrong flag op_mov_ptr_reg_zero_reg: {0}".format(asm.beforeSecondCall)
                    exit(0)
            asm.beforeSecondCall = None
            if asm.debug == True:
                print "asm.beforeSecondCall = None3"


def op_mov_ptr_reg_int_int(asm):
    register = asm.codeParams[0]
    offset = asm.registers[register] + asm.exe.read(asm.pos + asm.codeParams[1], 4, "i")
    val = asm.exe.read(asm.pos + asm.codeParams[2], 4, "i")
    asm.setMemory32(offset, val)


def op_inc_reg(asm):
    register = asm.codeParams[0]
    asm.addRegister(register, 1)


def op_call(asm):
    ptr = asm.exe.read(asm.pos + 1, 4, "I")
    addr = Math.sumOffset(asm.pos + 5, ptr)
    if asm.inJump == True:
        # emulate return value from function
        asm.registers["eax"] = 0
    if asm.runFlag == 0:
        if addr not in asm.callFunctions:
            asm.callFunctions[addr] = CallFunction(addr)
        asm.callFunctions[addr].addCall(asm)
        asm.pushCounter = 0
        asm.popCounter = 0
        asm.allCallAddrs.append(addr)
    elif asm.runFlag == 1:
        if addr in asm.allCallFunctions:
            func = asm.allCallFunctions[addr]
            if func.callCounter == 1:
                # probably alloc or other crap, need walk inside or emulate walking.
                if asm.lastRegValue is not None:
                    asm.addRegister("esp", asm.lastRegValue)
                    if asm.debug == True:
                        print "Alloca emulation. Moving esp to +{0}".format(hex(asm.lastRegValue))
                    asm.lastRegValue = True
                    asm.addRegister("esp", func.adjustSp)
                    return
        if addr in asm.callFunctions:
            func = asm.callFunctions[addr]
            func.collectArgs(asm)
            #asm.cxSet = False
            asm.addRegister("esp", func.adjustSp)
            #print "adjust stack: {0}".format(func.adjustSp)
        elif addr in asm.allCallFunctions:
            func = asm.allCallFunctions[addr]
            asm.addRegister("esp", func.adjustSp)
        else:
            asm.exe.log("Error. Need adjust stack based on function")
            exit(0)


def op_cmp_reg_int(asm):
    register = asm.codeParams[0]
    val1 = asm.registers[register]
    val2 = asm.exe.read(asm.pos + asm.codeParams[1], 4, "i")
    if val1 <= val2:
        asm.flags["le"] = True
    else:
        asm.flags["le"] = False


def op_jmp_le_byte(asm):
    asm.inJump = True
    if asm.flags["le"] == True:
        offset = asm.pos + asm.codeSize + asm.exe.read(asm.pos + asm.codeParams[0], 1, "b")
        asm.pos = offset
        asm.Jumped = True


def op_retn(asm):
    pass


class Asm:
    codes = (
        (2, [0x33, 0xd2],       "xor edx, edx",       2,  op_unset_reg,            ["edx"]),
        (2, [0x33, 0xf6],       "xor esi, esi",       2,  op_unset_reg,            ["esi"]),
        (2, [0x33, 0xff],       "xor edi, edi",       2,  op_unset_reg,            ["edi"]),
        (1, [0x3d],             "cmp eax, N",         5,  op_cmp_reg_int,          ["eax", 1]),
        (1, [0x40],             "inc eax",            1,  op_inc_reg,              ["eax"]),
        (1, [0x50],             "push eax",           1,  op_push_reg,             ["eax"]),
        (1, [0x51],             "push ecx",           1,  op_push_reg,             ["ecx"]),
        (1, [0x52],             "push edx",           1,  op_push_reg,             ["edx"]),
        (1, [0x53],             "push ebx",           1,  op_push_reg,             ["ebx"]),
        (1, [0x55],             "push ebp",           1,  op_push_reg,             ["ebp"]),
        (1, [0x56],             "push esp",           1,  op_push_reg,             ["esp"]),
        (1, [0x57],             "push edi",           1,  op_push_reg,             ["edi"]),
        (1, [0x59],             "pop ecx",            1,  op_pop_reg,              ["ecx"]),
        (1, [0x5b],             "pop ebx",            1,  op_pop_reg,              ["ebx"]),
        (1, [0x5d],             "pop ebp",            1,  op_pop_reg,              ["ebp"]),
        (1, [0x5e],             "pop esi",            1,  op_pop_reg,              ["esi"]),
        (1, [0x5f],             "pop edi",            1,  op_pop_reg,              ["edi"]),
        (1, [0x68],             "push NNN",           5,  op_push_big,             []),
        (1, [0x6a],             "push N",             2,  op_push_small,           []),
#        (1, [0x7e],             "jle A",              2,  op_jmp_le_byte,          [1]),
        (2, [0x83, 0xc0],       "add eax, N",         3,  op_add_reg_byte,         ["eax", 2]),
        (2, [0x83, 0xc4],       "add esp, N",         3,  op_add_reg_byte,         ["esp", 2]),
        (2, [0x83, 0xe4],       "and esp, 0xNNNNNN",  3,  op_and_reg_byte,         ["esp", 2]),
        (2, [0x83, 0xc8],       "or eax, 0xNNNNNN",   3,  op_or_reg_byte,          ["eax", 2]),
        (2, [0x83, 0xcb],       "or ebx, 0xNNNNNN",   3,  op_or_reg_byte,          ["ebx", 2]),
        (2, [0x83, 0xcf],       "or edi, 0xNNNNNN",   3,  op_or_reg_byte,          ["edi", 2]),
        (2, [0x81, 0xec],       "sub esp, NNN",       6,  op_sub_int,              ["esp", 2]),
        (2, [0x83, 0xec],       "sub esp, N",         3,  op_sub_byte,             ["esp", 2]),
        (2, [0x89, 0x18],       "mov [eax], ebx",     2,  op_mov_ptr_reg_zero_reg, ["eax", "ebx"]),
        (2, [0x89, 0x38],       "mov [eax], edi",     2,  op_mov_ptr_reg_zero_reg, ["eax", "edi"]),
        (3, [0x89, 0x44, 0x24], "mov [esp + N], eax", 4,  op_mov_ptr_reg_byte,     ["esp", "eax", 3]),
        (2, [0x89, 0x45],       "mov [ebp + N], eax", 3,  op_mov_ptr_reg_byte,     ["ebp", "eax", 2]),
        (2, [0x89, 0x48],       "mov [eax + N], ecx", 3,  op_mov_ptr_reg_byte,     ["eax", "ecx", 2]),
        (2, [0x89, 0x4d],       "mov [ebp + N], ecx", 3,  op_mov_ptr_reg_byte,     ["ebp", "ecx", 2]),
        (3, [0x89, 0x4c, 0x24], "mov [esp + N], ecx", 4,  op_mov_ptr_reg_byte,     ["esp", "ecx", 3]),
        (3, [0x89, 0x54, 0x24], "mov [esp + N], edx", 4,  op_mov_ptr_reg_byte,     ["esp", "edx", 3]),
        (2, [0x89, 0x58],       "mov [eax + N], ebx", 3,  op_mov_ptr_reg_byte,     ["eax", "ebx", 2]),
        (2, [0x89, 0x5d],       "mov [ebp + N], ebx", 3,  op_mov_ptr_reg_byte,     ["ebp", "ebx", 2]),
        (2, [0x89, 0x75],       "mov [ebp + N], esi", 3,  op_mov_ptr_reg_byte,     ["ebp", "esi", 2]),
        (2, [0x89, 0x78],       "mov [eax + N], edi", 3,  op_mov_ptr_reg_byte,     ["eax", "edi", 2]),
        (3, [0x89, 0x7c, 0x24], "mov [esp + N], edi", 4,  op_mov_ptr_reg_byte,     ["esp", "edi", 3]),
        (2, [0x89, 0x7d],       "mov [ebp + N], edi", 3,  op_mov_ptr_reg_byte,     ["ebp", "edi", 2]),
        (2, [0x89, 0x85],       "mov [ebp + N], eax", 6,  op_mov_ptr_reg_int,      ["ebp", "eax", 2]),
        (2, [0x89, 0x8D],       "mov [ebp + N], ecx", 6,  op_mov_ptr_reg_int,      ["ebp", "ecx", 2]),
        (2, [0x89, 0x95],       "mov [ebp + N], edx", 6,  op_mov_ptr_reg_int,      ["ebp", "edx", 2]),
        (2, [0x89, 0x9d],       "mov [ebp + N], ebx", 6,  op_mov_ptr_reg_int,      ["ebp", "ebx", 2]),
        (2, [0x89, 0xbd],       "mov [ebp + N], edi", 6,  op_mov_ptr_reg_int,      ["ebp", "edi", 2]),
        (2, [0x8b, 0x00],       "mov eax, [eax]",     2,  op_mov_reg_ptr_zero,     ["eax", "eax"]),
        (2, [0x8b, 0x02],       "mov eax, [edx]",     2,  op_mov_reg_ptr_zero,     ["eax", "edx"]),
        (2, [0x8b, 0x10],       "mov edx, [eax]",     2,  op_mov_reg_ptr_zero,     ["edx", "eax"]),
        (2, [0x8b, 0x40],       "mov eax, [eax + N]", 3,  op_mov_reg_ptr_byte,     ["eax", "eax", 2]),
        (2, [0x8b, 0x45],       "mov eax, [ebp + N]", 3,  op_mov_reg_ptr_byte,     ["eax", "ebp", 2]),
        (2, [0x8b, 0x48],       "mov ecx, [eax + N]", 3,  op_mov_reg_ptr_byte,     ["ecx", "eax", 2]),
        (2, [0x8b, 0x4a],       "mov ecx, [edx + N]", 3,  op_mov_reg_ptr_byte,     ["ecx", "edx", 2]),
        (2, [0x8b, 0x50],       "mov edx, [eax + N]", 3,  op_mov_reg_ptr_byte,     ["edx", "eax", 2]),
        (2, [0x8b, 0x52],       "mov edx, [edx + N]", 3,  op_mov_reg_ptr_byte,     ["edx", "edx", 2]),
        (2, [0x8b, 0x85],       "mov eax, [ebp + N]", 6,  op_mov_reg_ptr_int,      ["eax", "ebp", 2]),
        (2, [0x8b, 0x8D],       "mov ecx, [ebp + N]", 6,  op_mov_reg_ptr_int,      ["ecx", "ebp", 2]),
        (2, [0x8b, 0x95],       "mov edx, [ebp + N]", 6,  op_mov_reg_ptr_int,      ["edx", "ebp", 2]),
        (2, [0x8b, 0xc8],       "mov ecx, eax",       2,  op_mov_reg_reg,          ["ecx", "eax"]),
        (2, [0x8b, 0xce],       "mov ecx, esi",       2,  op_mov_reg_reg,          ["ecx", "esi"]),
        (2, [0x8b, 0xd0],       "mov edx, eax",       2,  op_mov_reg_reg,          ["edx", "eax"]),
        (2, [0x8b, 0xe5],       "mov esp, ebp",       2,  op_mov_reg_reg,          ["esp", "ebp"]),
        (2, [0x8b, 0xec],       "mov ebp, esp",       2,  op_mov_reg_reg,          ["ebp", "esp"]),
        (2, [0x8b, 0xf1],       "mov esi, ecx",       2,  op_mov_reg_reg,          ["esi", "ecx"]),
        (3, [0x8d, 0x04, 0x24], "lea eax, [esp]",     3,  op_lea_reg_zero,         ["eax", "esp"]),
        (3, [0x8d, 0x0c, 0x24], "lea ecx, [esp]",     3,  op_lea_reg_zero,         ["ecx", "esp"]),
        (3, [0x8d, 0x14, 0x24], "lea edx, [esp]",     3,  op_lea_reg_zero,         ["edx", "esp"]),
        (3, [0x8d, 0x44, 0x24], "lea eax, [esp + N]", 4,  op_lea_reg_byte,         ["eax", "esp", 3]),
        (2, [0x8d, 0x45],       "lea eax, [ebp + N]", 3,  op_lea_reg_byte,         ["eax", "ebp", 2]),
        (3, [0x8d, 0x4c, 0x24], "lea ecx, [esp + N]", 4,  op_lea_reg_byte,         ["ecx", "esp", 3]),
        (2, [0x8d, 0x4d],       "lea ecx, [ebp + N]", 3,  op_lea_reg_byte,         ["ecx", "ebp", 2]),
        (3, [0x8d, 0x54, 0x24], "lea edx, [esp + N]", 4,  op_lea_reg_byte,         ["edx", "esp", 3]),
        (2, [0x8d, 0x55],       "lea edx, [ebp + N]", 3,  op_lea_reg_byte,         ["edx", "ebp", 2]),
        (2, [0x8d, 0x71],       "lea esi, [ecx + N]", 3,  op_lea_reg_byte,         ["esi", "ecx", 2]),
        (2, [0x8d, 0x85],       "lea eax, [ebp + N]", 6,  op_lea_reg_int,          ["eax", "ebp", 2]),
        (2, [0x8d, 0x8d],       "lea ecx, [ebp + N]", 6,  op_lea_reg_int,          ["ecx", "ebp", 2]),
        (2, [0x8d, 0x95],       "lea edx, [ebp + N]", 6,  op_lea_reg_int,          ["edx", "ebp", 2]),
        (1, [0xb8],             "mov eax, N",         5,  op_mov_reg_val,          ["eax", 1]),
        (1, [0xb9],             "mov ecx, N",         5,  op_mov_reg_val,          ["ecx", 1]),
        (1, [0xba],             "mov edx, N",         5,  op_mov_reg_val,          ["edx", 1]),
        (1, [0xbb],             "mov ebx, N",         5,  op_mov_reg_val,          ["ebx", 1]),
        (1, [0xbf],             "mov edi, N",         5,  op_mov_reg_val,          ["edi", 1]),
        (1, [0xc3],             "retn",               1,  op_retn,                 []),
        (1, [0xc2],             "retn N",             3,  unimplimentedOpcode,     []),
        (1, [0xbe],             "mov esi, N",         5,  op_mov_reg_val,          ["esi", 1]),
        (2, [0xc7, 0x00],       "mov [eax], M",       6,  op_mov_ptr_reg_zero_int, ["eax", 2]),
        (3, [0xc7, 0x04, 0x24], "mov [esp], M",       7,  op_mov_ptr_reg_zero_int, ["esp", 3]),
        (2, [0xc7, 0x40],       "mov [eax + N], M",   7,  op_mov_ptr_reg_byte_int, ["eax", 2, 3]),
        (3, [0xc7, 0x44, 0x24], "mov [esp + N], M",   8,  op_mov_ptr_reg_byte_int, ["esp", 3, 4]),
        (2, [0xc7, 0x45],       "mov [ebp + N], M",   7,  op_mov_ptr_reg_byte_int, ["ebp", 2, 3]),
        (2, [0xc7, 0x85],       "mov [ebp + NN], M",  10, op_mov_ptr_reg_int_int,  ["ebp", 2, 6]),
        (1, [0xe8],             "call offset",        5,  op_call,                 []),
    )

    def __init__(self):
        self.pos = 0
        self.registers = dict()
        self.flags = dict()
        self.memory = dict()
        self.popCounter = 0
        self.pushCounter = 0
        self.runFlag = 0
        self.callFunctions = dict()
        self.stackAlign = 0
        self.packets = []
        self.debug = False
        self.debugMemory = False
        self.lastRegValue = None
        self.prevPacketId = 0
        self.prevLens = (0, 0)
        self.beforeSecondCall = None
        self.allCallAddrs = []
        self.cxSet = False
        # set if previous command was jump
        self.Jumped = False
        # set if any jumps was executed before
        self.inJump = False


    def getByte(self, pos):
        return self.exe.read(pos, 1, "B")


    def getWord(self, pos):
        return self.exe.read(pos, 2, "H")


    def decodeOpCode(self):
        self.codeText = self.code[2]
        self.codeSize = self.code[3]
        self.codeFunc = self.code[4]
        self.codeParams = self.code[5]


    def init(self, offset):
        self.pos = offset
        #print "start pos: {0}".format(self.pos)
        self.code = self.findOpcode()
        if self.code == False:
            cmd = self.getWord(self.pos)
            self.exe.log("Error: Unknown opcode: {0}".format(hex(cmd)))
            exit(1)
        self.decodeOpCode()


    def initState(self, runFlag):
        self.registers["eax"] = 0
        self.registers["ecx"] = 0x1000000
        self.registers["edx"] = 0x3000000
        self.registers["ebx"] = 0x5000000
        self.registers["esp"] = 0x7000000
        self.registers["ebp"] = 0x9000000
        self.registers["esi"] = 0xB000000
        self.registers["edi"] = 0xD000000
        self.runFlag = runFlag
        self.startEsp = self.registers["esp"]
        self.flags["le"] = False


    def findOpcode(self):
        for code in self.codes:
            cmdSize = code[0]
            bytes = code[1]
            found = True
            for ptr in range(0, cmdSize):
                if self.getByte(self.pos + ptr) != bytes[ptr]:
                    found = False
                    break
            if found == True:
                return code
        return False


    def getSize(self):
        return self.codeSize


    def inc(self, num):
        self.pos = self.pos + num


    def printLine(self):
        print "{0} {1}".format(self.exe.getAddrSec(self.pos), self.codeText)


    def printMemory(self):
        for key in self.memory:
            if key % 4 == 0:
                val = self.getMemory32(key)
                print "{0}: {1}, {2}".format(hex(key), hex(val), val)


    def printRegisters(self):
        for key in self.registers:
            print "{0}: {1}".format(key, hex(self.registers[key]))
        print "flags LE: {0}".format(self.flags["le"])


    def move(self):
        if self.codeText == "retn" or self.codeText == "retn N":
            return False
        if self.Jumped == False:
            self.inc(self.getSize())
        else:
            self.Jumped = False
        self.code = self.findOpcode()
        if self.code == False:
            cmd = self.getWord(self.pos)
            self.exe.log("Error: Unknown opcode: {0} {1}".format(self.exe.getAddrSec(self.pos), hex(cmd)))
            exit(1)
        self.decodeOpCode()
        return True


    def run(self):
        if self.debug == True:
            self.printLine()
        self.codeFunc(self)
        if self.debugMemory == True:
            self.printRegisters()
            self.printMemory()


    def printAll(self):
        self.printLine()
        self.printRegisters()
        self.printMemory()


    def setMemory32(self, addr, value):
        try:
            arr = struct.unpack("4B", struct.pack("I", value))
        except:
            arr = struct.unpack("4B", struct.pack("i", value))
        self.setMemoryData(addr, arr)


    def setMemoryData(self, addr, arr):
        for idx in range(0, len(arr)):
            self.memory[addr + idx] = arr[idx]


    def getMemory32(self, addr):
        if addr not in self.memory:
            return 0
        val = struct.unpack("I",
            struct.pack("4B",
            self.memory[addr],
            self.memory[addr + 1],
            self.memory[addr + 2],
            self.memory[addr + 3]
        ))[0]
        return val


    def getMemory32Sign(self, addr):
        if addr not in self.memory:
            return 0
        val = struct.unpack("i",
            struct.pack("4B",
            self.memory[addr],
            self.memory[addr + 1],
            self.memory[addr + 2],
            self.memory[addr + 3]
        ))[0]
        return val


    def addRegister(self, register, value):
        self.registers[register] = (self.registers[register] + value) & 0xffffffff


    def subRegister(self, register, value):
        self.registers[register] = (self.registers[register] - value) & 0xffffffff


    def andRegister(self, register, value):
        self.registers[register] = (self.registers[register] & value) & 0xffffffff


    def orRegister(self, register, value):
        self.registers[register] = (self.registers[register] | value) & 0xffffffff


    def setRegister(self, register, value):
        self.registers[register] = value & 0xffffffff


    def addPacket(self, packetId, len1, len2, flag):
        # fix negative -1
        if len1 == 255 and len1 != len2:
            len1 = -1
        elif len1 == 4294967295:
            len1 = -1
            if len2 == 4294967295:
                len2 = -1
        elif len1 == 117440492:
            len1 = -1
            if len2 == 117440492:
                len2 = -1
        if packetId < 0xb00 and packetId > 0 and flag >= -1 and flag <= 1 and len1 >= -1 and len2 >= -1 and len1 != 0 and len2 != 0 and len1 < 60000 and len2 < 60000:
            self.packets.append((
                packetId,
                len1,
                len2,
                flag
            ))
            if self.debug == True:
                print "packet found {0}: {1}, {2}, {3}".format(hex(packetId), len1, len2, flag)
        else:
            self.exe.log("Error: Wrong packet detected")
            self.exe.log("Wrong packet found {0}: {1}, {2}, {3}".format(hex(packetId), len1, len2, flag))
            exit(1)
