#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2011-2013 Yommys
# Copyright (C) 2016-2018 Andrei Karas (4144)

import datetime
import os
import struct

from src.section import Section


class Exe:
    def __init__(self):
        self.exe = None
        self.size = 0
        self.PEHeader = False
        self.image_base = 0
        self.client_date = 0
        self.sections = dict()
        self.themida = False
        self.fileName = ""
        self.logFile = None
        self.outDir = ""
        self.asprotect = False


    def log(self, text):
        self.logFile.write(text + "\n")
        print text


    def load(self, fileName, outFileName):
        outDir = "output/" + fileName
        with open("clients/" + fileName, "rb") as f:
            self.exe = f.read()
        self.size = len(self.exe)
        self.PEHeader = self.match("\x50\x45\x00\x00")
        if self.PEHeader == False:
            self.log("Error: Cant find PE header. Exiting.")
            exit(1)
        if fileName[-4:] == ".exe":
            fileName = fileName[:-4]
        self.fileName = fileName
        outDir = "output/" + fileName
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        self.logFile = open(outDir + "/" + outFileName, "wt")
        self.outDir = outDir
        self.image_base = self.read(self.PEHeader + 0x34, 4, "V")
        d = datetime.datetime.fromtimestamp(self.read(self.PEHeader + 8, 4, "V"))
        self.client_date = int(d.strftime("%Y")) * 10000 + int(d.strftime("%m")) * 100 + int(d.strftime("%d"))
        if self.client_date == 20041231:
            self.log("Broken client version detected. Fixing to correct one")
            self.client_date = 20120503
        self.log("client name: {0}".format(self.fileName))
        self.log("client date: {0}".format(self.client_date))
        sectionCount = self.read(self.PEHeader + 0x6, 2, "S");
        curSection = self.PEHeader + 0x18 + 0x60 + 0x10 * 0x8
        for idx in range(0, sectionCount):
            section = Section()
            section.name = self.read(curSection, 8).strip()
            section.image_base = self.image_base
            while section.name[-1:] == '\x00':
                section.name = section.name[:-1]
            if section.name == "" or section.name is None:
                section.name = "sect_" + str(idx)
                self.themida = True
            if section.name == ".adata":
                self.log("Warning: found .adata section. Binary probably encoded.")
                self.asprotect = True
            section.vSize         = self.read(curSection + 8 + 0 * 4, 4, "V")
            section.vOffset       = self.read(curSection + 8 + 1 * 4, 4, "V")
            section.vEnd          = section.vOffset + section.vSize
            section.rSize         = self.read(curSection + 8 + 2 * 4, 4, "V")
            section.rOffset       = self.read(curSection + 8 + 3 * 4, 4, "V")
            section.rEnd          = section.rOffset + section.rSize
            section.vrDiff        = section.vOffset - section.rOffset
            section.align         = 0
            self.sections[section.name] = section
            curSection = curSection + 0x28
        return True


    def close(self):
        self.logFile.close()
        self.logFile = None
        self.sections = None
        self.exe = None
        self.image_base = None


    def match(self, pattern, wildcard = "", start = 0, finish = -1):
        if finish == -1:
            finish = self.size
        else:
            if finish > self.size:
                finish = self.size
        if wildcard != "":
            for fpos in xrange(start, finish):
                found = True
                #print "fpos {0}".format(fpos)
                for ppos in xrange(0, len(pattern)):
                    #print "{0},{1} -> {2} vs {3} and {4}".format(fpos + ppos, ppos, self.exe[fpos + ppos], pattern[ppos], wildcard[0])
                    if self.exe[fpos + ppos] != pattern[ppos] and pattern[ppos] != wildcard:
                        found = False
                        break
                    #else:
                    #    print "same"
                if found == True:
                    #print "found"
                    return fpos
            return False
        else:
            ret = self.exe.find(pattern, start, finish)
            if ret == -1:
                return False
            return ret


    def matches(self, pattern, wildcard = "", start = 0, finish = -1):
        offsets = []
        # probably here dont need add len(pattern) ?
        offset = self.match(pattern, wildcard, start + len(pattern), finish)
        while offset != False:
            offsets.append(offset)
            offset = self.match(pattern, wildcard, offset + len(pattern), finish)
        if len(offsets) > 0:
            return offsets;
        return False;


    def read(self, offset, size, format = None):
        if offset >= self.size or self.size < 1:
            return False
        data = self.exe[offset:offset + size]
        if len(data) != size:
            self.log("Got wrong sized data in read")
            return False
        if format == "V" or format == "I":
            return struct.unpack("I", data)[0]
        elif format == "i":
            return struct.unpack("i", data)[0]
        elif format == "S" or format == "H":
            return struct.unpack("H", data)[0]
        elif format == "B":
            return struct.unpack("B", data)[0]
        elif format == "b":
            return struct.unpack("b", data)[0]
        elif format is not None:
            self.log("Error: wrong read format: {0}".format(format))
            exit(1)
        return data


    def detectSection(self, code, wildcard, cnt = 1):
        for key in self.sections:
            section = self.sections[key]
            print section.name
            offsets = self.matches(code, wildcard, section.rOffset, section.rOffset + section.rSize)
            if offsets == False:
                print "code found 0 matches"
                continue
            if cnt != -1 and len(offsets) != cnt:
                print "code found {0} matches".format(len(offsets))
                continue
            return section.name
        return False


    def getCodeSection(self):
        if self.themida == True:
            section = self.sections["sect_0"]
        else:
            section = self.sections[".text"]
        return section


    def code(self, code, wildcard, cnt = 1):
        section = self.getCodeSection()
        offsets = self.matches(code, wildcard, section.rOffset, section.rOffset + section.rSize)
        if offsets == False:
            print "code found 0 matches"
            return False
        if cnt != -1 and len(offsets) != cnt:
            print "code found {0} matches".format(len(offsets))
            return False
        if cnt == 1:
            return offsets[0]
        else:
            return offsets


    def code1(self, code, wildcard, start = -1, finish = -1):
        section = self.getCodeSection()
        if start == -1:
            start = section.rOffset
        if finish == -1:
            finish = section.rOffset + section.rSize
        offset = self.match(code, wildcard, start, finish)
        if offset == False:
            return False
        return offset


    def stringBySection(self, code, name):
        if name in self.sections:
            section = self.sections[name]
            offset = self.match("\x00" + code + "\x00", "", section.rOffset, section.rEnd)
            if offset != False:
                return offset + 1, section
        return False, None


    def string(self, code):
        codeSection = self.getCodeSection()
        offset = self.match("\x00" + code + "\x00", "", codeSection.rOffset, codeSection.rEnd)
        if offset != False:
            return offset + 1, codeSection
        offset, section = self.stringBySection(code, ".rdata")
        if offset != False:
            return offset, section
        offset, section = self.stringBySection(code, ".data")
        if offset != False:
            return offset, section
        for name in self.sections:
            if name == ".rdata" or name == ".data" or name == codeSection.name:
                continue
            offset, section2 = self.stringBySection(code, name)
            if offset != False:
                return offset + 1, section2
        return False, None


    def printRawAddr(self, text, offset):
        section = self.getCodeSection()
        self.log(section.printRawAddr(text, offset))


    def rawToVaHex(self, offset):
        section = self.getCodeSection()
        return hex(section.rawToVa(offset))


    def rawToVa(self, offset):
        section = self.getCodeSection()
        return section.rawToVa(offset)


    def getAddrSec(self, offset):
        section = self.getCodeSection()
        return section.getAddrSec(offset)


    def toHex(self, addr, sz):
        addr = struct.pack("I", addr)
        return ("00" * (sz - len(addr))) + addr


    def isClientTooOld(self):
        return self.client_date <= 20100824


    def isClientSkipFlag(self):
        return self.client_date >= 20100817
