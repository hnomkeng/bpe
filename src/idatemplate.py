#! /usr/bin/env python2
# -*- coding: utf8 -*-
#
# Copyright (C) 2016-2018 Andrei Karas (4144)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os


class IdaTemplate:
    def __init__(self, fileTemplate, lineTemplate):
        self.fileTemplate = self.loadTemplateFile(fileTemplate)
        self.lineTemplate = self.loadTemplateFile(lineTemplate)
        self.data = ""


    def loadTemplateFile(self, fileName):
        with open("templates/" + fileName, "rb") as f:
            return f.read()


    def addLine(self, addr, name):
        self.data = self.data + self.lineTemplate.format(addr, name.replace("::", "_"))


    def getData(self):
        return self.fileTemplate.format(self.data)


    def save(self, outDir, fileName, exeName):
        subName = os.path.basename(os.path.abspath("."))
        if not os.path.exists(outDir):
            os.makedirs(outDir)
        outName = outDir + "/" + fileName.format(subName, exeName)
        with open(outName, "wt") as w:
            w.write(self.getData())
